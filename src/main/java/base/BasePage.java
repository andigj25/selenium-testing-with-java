package base;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class BasePage {
    private String url;
    private Properties prop;
    public static String screenShotDestinationPath;

    public BasePage() throws IOException {

        // will be used to set the default url to visit and the type of browser

        prop = new Properties();
        FileInputStream data = new FileInputStream(
                System.getProperty("user.dir") + "\\src\\main\\java\\resources\\config.properties");
        prop.load(data);
    }

    // We declare this method here because we extend the BasePage in all our pageObjects
    // This way the WebElements in those classes will have access to the driver

    public static WebDriver getDriver() throws IOException {
        return WebDriverInstance.getDriver();
    }

    public String getUrl() throws IOException {
        url = prop.getProperty("url");
        return url;
    }

    // All the methods below are used for taking screenshots on test fails, correctly naming the file
    // and using it in our reports

    public static String takeScreenShot(String name) throws IOException {
        File srcFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        String dstFile = System.getProperty("user.dir") + "\\target\\screenshots\\"
                +timestamp()+ ".png";
        screenShotDestinationPath = dstFile;

        try {
            FileUtils.copyFile(srcFile, new File(dstFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return name;
    }

    public static String timestamp() {
        return new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());
    }

    public static String getScreenShotDestinationPath() {
        return screenShotDestinationPath;
    }

    // this method is for waiting for elements to not be visible
    public static void waitForElementInvisible(WebElement element, int timer) throws IOException {
        WebDriverWait wait = new WebDriverWait(getDriver(), timer);
        wait.until(ExpectedConditions.invisibilityOf(element));
    }
}
