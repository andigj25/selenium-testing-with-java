package resources;

import base.BasePage;
import base.ExtentManager;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.IOException;

// when implementing ITestListener the scope tag should be removed from the testng repo we added as a dependency

public class Listeners extends BasePage implements ITestListener {

    public Listeners() throws IOException {
        super();
    }
    // we will use onTestFailure, as we want to take a screenshot whenever a test fails

    // there was an error in the takeScreenShot method, as it took as a parameter a Webdriver
    // we amend it to take as a parameter a string, as the method in the try/catch we are using needs a string

    // ITestContext contains all the information for a given test, as opposed to just the result
    public synchronized void onStart(ITestContext context) {
        ExtentManager.getReport();
        ExtentManager.createTest(context.getName(), context.getName());
    }

    public synchronized void onTestFailure(ITestResult result) {
        ExtentManager.getTest().fail(result.getThrowable());
        try {
            System.out.println("Test failed: " + result.getName());
            takeScreenShot(result.getMethod().getMethodName());
            ExtentManager.attachImage();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // IMPORTANT to add the @Listener annotation below each test we want to run after the imports:
        // @Listeners(resources.Listeners.class)
    }

    public synchronized void onFinish(ITestContext context) {
        ExtentManager.flushReport();
    }

}
