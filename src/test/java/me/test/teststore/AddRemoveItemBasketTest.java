package me.test.teststore;

import base.ExtentManager;
import base.Hooks;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObjects.*;

import java.io.IOException;

@Listeners(resources.Listeners.class)

public class AddRemoveItemBasketTest extends Hooks {
    public AddRemoveItemBasketTest() throws IOException {
        super();
    }

    @Test
    public void addRemoveItem() throws IOException {

        ExtentManager.log("Starting AddRemoveItemBasketTest...");

        Homepage home = new Homepage();
        home.getTestStoreLink().click();

        ShopHomepage shopHome = new ShopHomepage();
        ExtentManager.pass("Reached the shop homepage");
        shopHome.getProdOne().click();

        ShopProductPage shopProd = new ShopProductPage();
        ExtentManager.pass("Reached the shop product page");
        Select option = new Select(shopProd.getSizeOption());
        option.selectByVisibleText("M");
        ExtentManager.pass("Have successfully selected product size");
        shopProd.getQuantIncrease().click();
        ExtentManager.pass("Have successfully selected increased quantity");
        shopProd.getAddToCartBtn().click();
        ExtentManager.pass("Have successfully added product to basket");

        ShopContentPanel cPanel = new ShopContentPanel();
        cPanel.getContinueShopBtn().click();

        shopProd.getHomepageLink().click();
        shopHome.getProdTwo().click();
        shopProd.getAddToCartBtn().click();
        cPanel.getCheckoutBtn().click();

        // we need to crate now an object for the Shopping Cart Page
        ShoppingCart cart = new ShoppingCart();
        cart.getDeleteItemTwo().click();

        // We add an explicit wait as the total amount we get is read before the second item
        // and we can't get the correct amount to compare with the expected value

        waitForElementInvisible(cart.getDeleteItemTwo(), 10);

        // we assert if the total amount is what we expect
        try {
            Assert.assertEquals(cart.getTotalAmount().getText(), "$45.24");
            ExtentManager.pass("The total amount matches the expected amount");
        } catch (AssertionError e) {
            Assert.fail("Cart amount did not match the expected amount, it found" + cart.getTotalAmount().getText() +
                    "| expected amount: $45.24");
            ExtentManager.fail("The total amount did not match the expected amount.");
        }

    }

}
