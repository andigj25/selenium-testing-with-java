package me.test.teststore;

import base.ExtentManager;
import base.Hooks;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import pageObjects.*;
import java.io.IOException;

@Listeners(resources.Listeners.class)

// We extend the Hooks class, which on its own extends the BasePage
// This way we have access to everything we need
public class OrderCompleteTest extends Hooks {

    // because we are extending the Hooks class (which contains a constructor) we need to add a constructor
    // and add the super keyword so we can inherit the methods of that class
    public OrderCompleteTest() throws IOException {
        super();
    }

    @Test
    public void endToEndTest() throws IOException {
        // to interact with the objects of the HomePage we need to create a new object

        ExtentManager.log("Starting OrderCompleteTest...");

        Homepage home = new Homepage();
        home.getTestStoreLink().click();
        ExtentManager.pass("Have successfully reached the store homepage");

        ShopHomepage shopHome = new ShopHomepage();
        shopHome.getProdOne().click();
        ExtentManager.pass("Have successfully clicked on product");

        ShopProductPage shopProd = new ShopProductPage();
        ExtentManager.pass("Have successfully reached the shop product page");

        // when interacting with dropdown menus the Select class must be used
        // We pass the value of the locator of the dropdown menu using the object we created

        Select option = new Select(shopProd.getSizeOption());
        option.selectByVisibleText("M");
        ExtentManager.pass("Have successfully selected product size");
        shopProd.getQuantIncrease().click();
        ExtentManager.pass("Have successfully increased quantity");
        shopProd.getAddToCartBtn().click();
        ExtentManager.pass("Have successfully added item to cart");

        ShopContentPanel cPanel = new ShopContentPanel();
        cPanel.getCheckoutBtn().click();

        ShoppingCart cart = new ShoppingCart();
        cart.getHavePromo().click();
        ExtentManager.pass("Have successfully selected the promo button");
        cart.getPromoTextbox().sendKeys("20OFF");
        cart.getPromoAddBtn().click();
        cart.getProceedCheckoutBtn().click();
        ExtentManager.pass("Have successfully selected the checkout button");

        OrderFormPersInfo pInfo = new OrderFormPersInfo();
        pInfo.getGenderMr().click();
        pInfo.getFirstNameField().sendKeys("John");
        pInfo.getLastnameField().sendKeys("Smith");
        pInfo.getEmailField().sendKeys("johnsmith1@test.com");
        pInfo.getTermsConditionsCheckbox().click();
        pInfo.getContinueBtn().click();
        ExtentManager.pass("Have successfully entered customer details");

        OrderFormDelivery orderDelivery = new OrderFormDelivery();
        orderDelivery.getAddressField().sendKeys("Main Street");
        orderDelivery.getCityField().sendKeys("Huston");
        Select state = new Select(orderDelivery.getStateDropdown());
        state.selectByVisibleText("Texas");
        orderDelivery.getPostcodeField().sendKeys("10001");
        orderDelivery.getContinueBtn().click();
        ExtentManager.pass("Have successfully entered delivery information");

        OrderFormShippingMethod shipMethod = new OrderFormShippingMethod();
        shipMethod.getDeliveryMsgTextbox().sendKeys("If I am not in, please leave my delivery on my porch.");
        shipMethod.getContinueBtn().click();
        ExtentManager.pass("Have successfully selected the shipping method");

        OrderFormPayment orderPay = new OrderFormPayment();
        orderPay.getPayByCheckRadioBtn().click();
        orderPay.getTermsConditionsCheckbox().click();
        orderPay.getOrderBtn().click();
        ExtentManager.pass("Have successfully placed order");
    }
}
