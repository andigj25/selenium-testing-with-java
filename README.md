# Automation testing with Selenium and Java

## Disclaimer

This project is made following the instruction given in the Udemy Course: [Selenium Webdriver & Java - Using real examples](https://www.udemy.com/share/106HOK3@9O4zznnw1UVcswxU6lSOvLmTeJ24-hkuJXp1GzmcD-_mX3j7mL5-xYzSrDRCl0HpZw==/). Its contents are based on the step by step instructions given by the instructor throughout the lectures. The project was made with the aim of getting hands-on experience with automation testing and to exibhit the knowledge gained throughout the course. 

## Description

The project was built using Maven and is a framework for running automated tests using Selenium and Java. The framework has been arranged to specifically run tests for the webpage https://automationtesting.co.uk/ and includes two tests which can run separately, or in parallel using TestNG.

The main advantage of this framweork is that it can be reused to run tests on other webpages. 

As highlighted above, it supports parallell running of tests and autogenerates html reports for each test that can be easily read and understood by the whole team.

This project applies concepts such as inheritance, encapsulation and constructors with Java. At the same time it highlight the good practices of parametrization and reusability of code and classes, which allows for easy amendemend of existing tests in cases of changes to the webpage and easy reusability of the same code for running tests on different webpages/projects.

## Supported browsers

The project supports running tests in Google Chrome, Mozilla Firefox and Microsoft Edge. The browser can be selected by using the config.properties file in the following the path:

src > main > java > resources > config.properties

In the browser field you have to input either: chrome, firefox, or edge. Google Chrome is selected by default.

Please note that for the tests to run the browser version needs to match the driver version of the selected version. The drivers of this project can be found by following this path:

src > main > java > drivers

To update the current version of the drivers inside the project you may visit the pages below and download the corresponding version with your driver and your operating system. The default webdrivers support 64 bit Windows devices.

- [ ] [ChromeDriver for Google Chrome](https://chromedriver.chromium.org/downloads)   | Version in the project: 106.0.5249.61
- [ ] [GeckoDriver for Firefox](https://github.com/mozilla/geckodriver/releases)   | Version in the project: v0.32.0. 
- [ ] [MSEdgeDriver for Edge](https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/)   | Version in the project: 106.0.1370.42

## Included tests

They can be found in the following path:
src > test > java > me.test.teststore

1. AddRemoveItemTest

The test aims to check common functionalities of a e-commerce websites. It checks if we can add product to cart, change size, change quantity, delete products from the cart and asserts if the price is correct.

This test takes the following steps:
- [ ] Goes to https://automationtesting.co.uk/
- [ ] Clicks the Test Store link on the left pane
- [ ] Clicks on the Hummingbird Printed T-Shirt product
- [ ] Changes size to M
- [ ] Increases quantity to 2
- [ ] Adds product to cart
- [ ] Goes back to the store Homepage
- [ ] Clicks on the Hummingbird Printed Sweater
- [ ] Add product to cart
- [ ] Continues to checkout
- [ ] Deletes the second product that was added
- [ ] Asserts if the price that is shown in the checkout is correct

2. OrderCompleteTest

The test simulates the full experience of buying a product from a user. This includes the selecting of products, checking out, filling all the required fields and finalizing the order. 

This test takes the following steps:
- [ ] Goes to https://automationtesting.co.uk/
- [ ] Clicks the Test Store link on the left pane
- [ ] Clicks on the Hummingbird Printed T-Shirt product
- [ ] Changes size to M
- [ ] Increases quantity to 2
- [ ] Adds product to cart
- [ ] Adds a 20% off code
- [ ] Checksout
- [ ] Inputs personal info 
- [ ] Inputs Delivery info 
- [ ] Inputs Shipping Method info and delivery instructions
- [ ] Selects "Pay by check" method
- [ ] Checks Terms and Conditions checkbox
- [ ] Places order

## Setup
The project was coded using Maven and Java 11. Dependencies for this project include:

junit,
selenium,
testng,
commons-io,
aventstack for extentreports

Plugins include:

maven-surefire-plugin for testng

## Installation
The project can be copied from the repository and opened in your machine that has Java installed. 

When using IntelliJ IDEA it is only necessary to open the POM file in the IDE and IntelliJ will recognize the project and import it accordingly. Please be sure that Maven has downloaded all the necessary dependencies before running the test. 

If importing the project in Eclipse, please find instruction by clicking [HERE](https://www.lagomframework.com/documentation/1.6.x/java/EclipseMavenInt.html)

## TestNG
The projects supports running tests with TestNG, which allows for building test suits in accordance to the testers need. By default, the project comes with an existing TestNG xml file, which can be found directly in the main directory.

When run the testng.xml file executes the two included tests in parallell, as the project supports parallell testing.  

## Reports
Each time a test is run an html file is automatically generated in the reports folder on the main directory. The folder can be found at:

AutomationTesting > Reports

Reports are generated using the ExtentReport class from aventstack. It's main features are:

- [ ] Can be renamed in order to make it possible to distinguish different test runs.
- [ ] Automatically generates a timestamp in the file name to distinguish test runs from each other.
- [ ] Can break down tests in steps for easily distinguishing the steps that pass and fail through ExtentManager integration
- [ ] Includes charts with data on the outcome of tests.
- [ ] Generates a screenshot on test fail, which is automatically included in the html report.
- [ ] Generates a single file when running parallell tests, in order to have all the info in one place. 

***
